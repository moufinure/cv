<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:cv="http://www.moufinure.org/cv"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.moufinure.org/cv cv.xsd" xml:lang="fr">
	<xsl:output method="xml" encoding="UTF-8" indent="no"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" />
	<xsl:template match="cv:address">
	</xsl:template>

	<xsl:template match="cv:education">
		<div id="education">
			<h2>Éducation/parcours scolaire</h2>
			<ul>
				<xsl:for-each select="cv:qualification">
					<li>
						<p>
							<xsl:value-of select="@year" />
							:
							<xsl:value-of select="cv:description" />
						</p>
					</li>
				</xsl:for-each>
			</ul>
		</div>
	</xsl:template>

	<xsl:template match="cv:employment">
		<div id="employment">
			<h2>Parcours professionnel</h2>
			<ul>
				<xsl:for-each select="cv:job">
					<li>
						<p>
							Du
							<xsl:value-of select="@start" />
							au
							<xsl:value-of select="@end" />
							:
							<xsl:value-of select="@title" />
						</p>
						<p>
							<xsl:value-of select="cv:description" />
						</p>
					</li>
				</xsl:for-each>
			</ul>
		</div>
	</xsl:template>

	<xsl:template match="cv:interests">
		<div id="hobbiesandinterests">
			<h2>Hobbies et centres d'intérêt</h2>
			<ul>
				<xsl:for-each select="cv:interest">
					<li>
						<p>
							<xsl:value-of select="." />
						</p>
					</li>
				</xsl:for-each>
			</ul>
		</div>
	</xsl:template>

	<xsl:template match="cv:reference">
		<li>
			<p>
				<xsl:value-of select="." />
			</p>
		</li>
	</xsl:template>

	<xsl:template match="cv:references">
		<div id="references">
			<h2>Références</h2>
			<ul>
				<xsl:for-each select="cv:reference">
					<xsl:apply-templates />
				</xsl:for-each>
			</ul>
		</div>
	</xsl:template>

	<xsl:template match="cv:misc">
		<div id="misc">
		</div>
	</xsl:template>

	<xsl:template match="cv:person">
		<div id="me">
			<h1>
				<xsl:value-of select="@name" />
			</h1>
			<p>
				Né le
				<xsl:value-of select="@birth" />
			</p>
			<div id="contacts">
				<ul id="contactdetails">
					<xsl:for-each select="cv:contact">
						<xsl:choose>
							<xsl:when test="@type = mobile">
								<li>
									<p>
										Téléphone mobile:
										<xsl:apply-templates select="cv:contact" />
									</p>
								</li>
							</xsl:when>
							<xsl:when test="type = landline">
								<li>
									<p>
										Téléphone fixe:
										<xsl:apply-templates select="cv:contact" />
									</p>
								</li>
							</xsl:when>
							<xsl:when test="type = email">
								<li>
									<p>
										Email:
										<xsl:value-of select="cv:contact" />
									</p>
								</li>
							</xsl:when>
						</xsl:choose>
					</xsl:for-each>
				</ul>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="cv:resume">
		<div id="container">
			<div id="content">
				<div>
					<xsl:apply-templates select="cv:person" />
				</div>
				<h1 id="title">
					<xsl:value-of select="cv:title" />
				</h1>
				<div class="sxscontainer">
					<xsl:apply-templates select="cv:education" />
					<xsl:apply-templates select="cv:employment" />
				</div>
				<div class="sxscontainer">
					<xsl:apply-templates select="cv:interests" />
					<xsl:apply-templates select="cv:references" />
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="/cv:resume">
		<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<title>
					CV:
					<xsl:value-of select="cv:title" />
				</title>
				<link rel="stylesheet" type="text/css" href="cv.css" />
			</head>
			<body>
				<div id="container">
					<xsl:apply-templates select="." />
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>