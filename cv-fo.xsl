<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:cv="http://www.moufinure.org/cv"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.moufinure.org/cv cv.xsd">
	<xsl:output method="xml" media-type="text/xml" />

	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:page-sequence-master
					master-name="cv-main-sequence">
					<fo:single-page-master-reference
						master-reference="cv-a4-master" />
				</fo:page-sequence-master>
				<fo:simple-page-master
					master-name="cv-a4-master" page-height="29.7cm" page-width="21cm"
					margin-top="5mm" margin-bottom="10mm" margin-left="15mm"
					margin-right="15mm">
					<fo:region-body country="FR"
						region-name="cv-main-region" />
					<fo:region-start country="FR"
						region-name="cv-person-region" />
				</fo:simple-page-master>
			</fo:layout-master-set>

			<fo:page-sequence
				master-reference="cv-main-sequence" background="aliceblue">
				<fo:flow flow-name="cv-main-region" language="FR">
					<fo:block margin-left="0" margin-top="0"
						background-color="aliceblue" >
						<fo:block font-size="xx-large" color="gray"
							text-align="center">
							<xsl:value-of select="cv:resume/cv:person/@name" />
						</fo:block>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
</xsl:stylesheet>